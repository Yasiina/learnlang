package ru.itis.llang.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.StringTokenizer;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.fragments.base.BaseFragment;
import ru.itis.llang.model.Words;
import ru.itis.llang.paint.MainPaint_Fragment;
import ru.itis.llang.repositories.DictionaryRepository;
import ru.itis.llang.repositories.WordsRepository;
import ru.itis.llang.utils.CurrentSettings;


public class AddWordsFragment extends Fragment implements View.OnClickListener{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String PIC_PARAM = "picture";
    private String mParam1;
    private String mParam2;

    public static final String ADD_WORDS_FRAGMENT_TAG = "add_WORDS_fragment";

    private String firstL, secondL, name;
    private DictionaryRepository dicRepository;
    private static ImageView mImage;
    private static byte[] words_image = null;
    private WordsRepository wordsRepository;
    private String tableName = null;
    private View view;
    private boolean isHavePicture;
    private BaseFragment mBaseFragment;

    private OnFragmentInteractionListener mListener;

    public AddWordsFragment(){}

    @SuppressLint("ValidFragment")
    private AddWordsFragment(boolean isHavePicture) {
        this.isHavePicture = isHavePicture;
        Log.d(ADD_WORDS_FRAGMENT_TAG, "i'm in AddWordsFragment isPic=" + this.isHavePicture);
    }

    public static AddWordsFragment newInstance() {
        AddWordsFragment fragment = new AddWordsFragment(false);
        return fragment;
    }

    public static AddWordsFragment newInstance(byte[] mPic) {
        AddWordsFragment fragment = new AddWordsFragment(true);
        Bundle args = new Bundle();
        words_image = mPic;
        fragment.setArguments(args);
        mImage.setImageResource(R.drawable.checkmark);
        return fragment;
    }
    private class ViewHolder{
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        //EditText mTxtFirstLang, mTxtSecondLang, mTxtExplanation;
        AutoCompleteTextView  mTxtFirstLang, mTxtSecondLang, mTxtExplanation;
        Button mBtnAdd;
        //TextView tvFirstL, tvSecondL;
    }
    private ViewHolder vh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(ADD_WORDS_FRAGMENT_TAG, "i'm in create view isPic=" + isHavePicture);
        tableName = CurrentSettings.get_current_languages(getActivity().getBaseContext());
        Log.d(ADD_WORDS_FRAGMENT_TAG, tableName);
        view = inflater.inflate(R.layout.fragment_add_words, container, false);
        initView();

        if (!isNewDictionary())
        {
            int id = getActivity().getIntent().getExtras().getInt("id");
            WordsRepository w = new WordsRepository(getActivity().getApplicationContext());
            Words word = w.getWordById(id);
            /*vh.tvFirstL.setText(word.getFirstLang());
            vh.tvSecondL.setText(word.getSecondLang());*/
            vh.mTxtFirstLang.setHint(word.getFirstLang());
            vh.mTxtSecondLang.setHint(word.getSecondLang());
            vh.mTxtExplanation.setHint(word.getExplanation());
        }

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnADD_addWordActivity:

                this.wordsRepository = new WordsRepository(getActivity().getApplicationContext(), tableName);


                if (vh.mTxtFirstLang != null && vh.mTxtSecondLang != null && vh.mTxtExplanation != null
                        && mImage != null) {

                    Words createdWordsPair = new Words(
                            vh.mTxtFirstLang.getText().toString(),
                            vh.mTxtSecondLang.getText().toString(), words_image,
                            vh.mTxtExplanation.getText().toString());

                    if(tableName != null){
                        wordsRepository.addWordToTheme(tableName, createdWordsPair);
                    }


                    Log.d("ADD WORD", "added word pair : " + createdWordsPair.getmId());

                    Toast.makeText(getActivity().getApplicationContext(), "Successfully added new words", Toast.LENGTH_LONG).show();
                    vh.mTxtFirstLang.setText("");
                    vh.mTxtSecondLang.setText("");
                    vh.mTxtExplanation.setText("");
                    mImage.setImageResource(R.drawable.paint);
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "empty_fields_message", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.ivPictureOfWord:
                MainPaint_Fragment mFragment = MainPaint_Fragment.newInstance();
                FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
                mFragmentTransaction.replace(R.id.fl_add_words, mFragment,
                        MainPaint_Fragment.MAIN_PAINT_FRAGMENT_TAG);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                //mBaseFragment.replaceFragment(mFragment, MainPaint_Fragment.MAIN_PAINT_FRAGMENT_TAG, R.id.fl_add_words);
                break;
            default:
                break;

        }

    }

    private boolean isNewDictionary(){

        try {

            wordsRepository = new WordsRepository(getActivity().getApplicationContext());
            dicRepository = new DictionaryRepository(getActivity().getApplicationContext());
            StringTokenizer tokenizer = new StringTokenizer(tableName,"_");

            while (tokenizer.hasMoreElements()) {
                firstL = tokenizer.nextToken();
                secondL = tokenizer.nextToken();
            }
            /*vh.tvFirstL.setText(firstL);
            vh.tvSecondL.setText(secondL);*/
            vh.mTxtSecondLang.setHint(secondL);


            return true;

        }catch (Exception e1){
            return false;
        }
    }
    private void initView(){
        vh = new ViewHolder();
        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        /*vh.tvFirstL = (TextView) view.findViewById(R.id.tvFirstLangAddWord);
        vh.tvSecondL = (TextView) view.findViewById(R.id.tvSecondLangAddWord);*/

        /*vh.mTxtFirstLang = (EditText) view.findViewById(R.id.etNewWord);
        vh.mTxtSecondLang = (EditText) view.findViewById(R.id.etNewWordTranslate);
        vh.mTxtExplanation =  (EditText) view.findViewById(R.id.etNewWordAssociation);*/
        vh.mTxtFirstLang = (AutoCompleteTextView) view.findViewById(R.id.first_language_add_words);
        vh.mTxtSecondLang = (AutoCompleteTextView) view.findViewById(R.id.second_language_add_words);
        vh.mTxtExplanation =  (AutoCompleteTextView) view.findViewById(R.id.explanation_add_words);
        mImage = (ImageView) view.findViewById(R.id.ivPictureOfWord);
        mImage.setOnClickListener(this);
        vh.mBtnAdd = (Button) view.findViewById(R.id.btnADD_addWordActivity);
        vh.mBtnAdd.setOnClickListener(this);

        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_add_words));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();
    }
}
