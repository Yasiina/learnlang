package ru.itis.llang.fragments.alarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.itis.llang.R;
import ru.itis.llang.database.DBHelper;
import ru.itis.llang.utils.CurrentSettings;

public class AlarmDetailsActivity extends Activity {

    private AlarmModel alarm;
    private TextView txtToneSelection;
    private Context mContext;

    private EditText et_fromHours, et_fromMinutes, et_toHours, et_toMinutes, et_repeatTime,
    et_fromDay, et_fromYear, et_toDay, et_toYear, et_fromHours_sleep, et_fromMinutes_sleep, et_toHours_sleep, et_toMinutes_sleep;

    private Spinner timeMonth_from, timeMonth_to, timeAM_PM_from, timeAM_PM_to, timeAM_PM_from_sleep, timeAM_PM_to_sleep,
            repeat_min_hour;
    private TextView theme_name;
    private Button saveBtn;
    private int cur;
    private String name;
    private Uri alarmTone;
    private int fromAM_PM,  fromMonth, toAM_PM, toMonth, fromAM_PM_sleep, toAM_PM_sleep, rep_min_hour;
    private boolean old_alarm = false;
    private AlarmDAO alarmDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_details2);
        setContentView(R.layout.temp_alarm_details);

        DBHelper b = new DBHelper(this);
        SQLiteDatabase db = b.getWritableDatabase();
        db.execSQL("DROP TABLE alarmTable");

        mContext = getBaseContext();

        alarmDAO = new AlarmDAO(this);


            theme_name = (TextView) findViewById(R.id.textView_themeName_Alarm);
            name = CurrentSettings.get_current_theme(getBaseContext());
            String temp = name.replace("_theme","");
            theme_name.setText(temp);


        try{
            alarm = alarmDAO.get(name);
            txtToneSelection.setText(RingtoneManager.getRingtone(this, Uri.parse(alarm.alarmTone)).getTitle(this));
            old_alarm = true;
            Log.d("alala", "old alarm");

        }catch (RuntimeException e){
            alarm = new AlarmModel();
            alarm.setThemeName(name);
            old_alarm = false;
            Log.d("alala", "new alarm");

        }
        cur = 0;


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1: {
                    alarm.alarmTone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI).toString();
                    txtToneSelection.setText(RingtoneManager.getRingtone(this, alarmTone).getTitle(this));
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_alarm_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_save_alarm_details: {
                 setAlarmParameters();

                 AlarmManagerHelper.cancelAlarms(this);

                if(old_alarm) {
                    alarmDAO.updateAlarm(alarm);
                    Log.d("alala", "update old alarm");
                }
                else {
                    alarmDAO.createAlarm(alarm);
                    Log.d("alala", "create new alarm");
                }
                AlarmManagerHelper.setAlarms(this);
                alarmDAO.close();
                setResult(RESULT_OK);
                finish();
            }
        }

       return super.onOptionsItemSelected(item);
    }


    private void setAlarmParameters(){

        int fromHours = Integer.parseInt(et_fromHours.getText().toString());
        int fromMinutes = Integer.parseInt(et_fromMinutes.getText().toString());
        int fromDay =  Integer.parseInt(et_fromDay.getText().toString());
        int fromYear =  Integer.parseInt(et_fromYear.getText().toString());
        String fromAM_PM1 = timeAM_PM_from.getSelectedItem().toString();


        alarm.setFromHours(fromHours);
        alarm.setFromMinutes(fromMinutes);
        alarm.setFromDay(fromDay);
        alarm.setFromYear(fromYear);
        alarm.setFromAM_PM(fromAM_PM1);

        int toHours = Integer.parseInt(et_toHours.getText().toString());
        int toMinutes = Integer.parseInt(et_toMinutes.getText().toString());
        int toDay = Integer.parseInt(et_toDay.getText().toString());
        int toYear = Integer.parseInt(et_toYear.getText().toString());
        String toAM_PM1 = timeAM_PM_to.getSelectedItem().toString();

        int fromMonth = timeMonth_from.getSelectedItemPosition();
        alarm.setFromMonth(fromMonth);
        Log.d("alala", "fromMonth  " + fromMonth);

        int toMonth = timeMonth_to.getSelectedItemPosition();
        alarm.setToMonth(toMonth);
        Log.d("alala", "toMonth  " + toMonth);

        alarm.setToHours(toHours);
        alarm.setToMinutes(toMinutes);
        alarm.setToDay(toDay);
        alarm.setToYear(toYear);
        alarm.setToAM_PM(toAM_PM1);

        int fromHours_sleep = Integer.parseInt(et_fromHours_sleep.getText().toString());
        int fromMinutes_sleep = Integer.parseInt(et_fromMinutes_sleep.getText().toString());
        String fromAM_PM_sleep = timeAM_PM_from_sleep.getSelectedItem().toString();
        int toHours_sleep = Integer.parseInt(et_toHours_sleep.getText().toString());
        int toMinutes_sleep = Integer.parseInt(et_toMinutes_sleep.getText().toString());
        String toAM_PM_sleep = timeAM_PM_to_sleep.getSelectedItem().toString();

        alarm.setFromSleepHours(fromHours_sleep);
        alarm.setFromSleepMinutes(fromMinutes_sleep);
        alarm.setFromSleep_AM_PM(fromAM_PM_sleep);
        alarm.setToSleepHours(toHours_sleep);
        alarm.setToSleepMinutes(toMinutes_sleep);
        alarm.setToSleep_AM_PM(toAM_PM_sleep);

        int repeat = Integer.parseInt(et_repeatTime.getText().toString());
        if(rep_min_hour == 1) repeat = repeat*60;
        alarm.setRepeat(repeat);
        String rep = repeat_min_hour.getSelectedItem().toString();
        alarm.setRepeatMin_Hour(rep);

        alarm.setEnabled(true);

    }

    private void initView(){

        /*repeat_min_hour = (Spinner) findViewById(R.id.spinner_repeat_min_hour);
        et_fromHours = (EditText) findViewById(R.id.editText_hours);
        et_fromMinutes = (EditText) findViewById(R.id.editText_minutes);
        et_toHours = (EditText) findViewById(R.id.editText_hours_TO);
        et_toMinutes = (EditText) findViewById(R.id.editText_minutes_TO);
        et_repeatTime = (EditText) findViewById(R.id.editText_repeat);
        et_fromDay = (EditText) findViewById(R.id.editText_fromDay);
        timeMonth_from = (Spinner) findViewById(R.id.spinner_fromMonth);
        timeAM_PM_from = (Spinner) findViewById(R.id.spinner_am_pm_From);
        et_fromYear  = (EditText) findViewById(R.id.editText_fromYear);
        et_toDay  = (EditText) findViewById(R.id.editText_ToDay);
        timeMonth_to = (Spinner) findViewById(R.id.spinner_ToMonth);
        et_toYear  = (EditText) findViewById(R.id.editText_ToYear);
        timeAM_PM_to = (Spinner) findViewById(R.id.spinner_am_pm_To);
        timeAM_PM_from_sleep =  (Spinner) findViewById(R.id.spinner_am_pm_sleepFrom);
        timeAM_PM_to_sleep = (Spinner) findViewById(R.id.spinner_am_pm_sleepTO);
        et_fromHours_sleep = (EditText) findViewById(R.id.editText_hours_sleepFrom);
        et_fromMinutes_sleep = (EditText) findViewById(R.id.editText_minutes_sleepFrom);
        et_toHours_sleep = (EditText) findViewById(R.id.editText_hours_sleepTO);
        et_toMinutes_sleep = (EditText) findViewById(R.id.editText_minutes_sleepTO);
        txtToneSelection = (TextView) findViewById(R.id.alarm_label_tone_selection);*/

        saveBtn = (Button) findViewById(R.id.save_alarm_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlarmParameters();

                AlarmManagerHelper.cancelAlarms(mContext);

                if(old_alarm) {
                    alarmDAO.updateAlarm(alarm);
                    Log.d("alala", "update old alarm");
                }
                else {
                    alarmDAO.createAlarm(alarm);
                    Log.d("alala", "create new alarm");
                }
                AlarmManagerHelper.setAlarms(mContext);
                alarmDAO.close();
                setResult(RESULT_OK);
                finish();
            }
        });

        Calendar currentTime = new GregorianCalendar();
        et_fromHours.setText(currentTime.get(Calendar.HOUR)+"");
        et_fromMinutes.setText(currentTime.get(Calendar.MINUTE)+"");
        et_fromDay.setText(currentTime.get(Calendar.DAY_OF_MONTH)+"");
        et_fromYear.setText(currentTime.get(Calendar.YEAR)+"");
        int am_pm = currentTime.get(Calendar.AM_PM);
        timeAM_PM_from.setSelection(am_pm);
        int month = currentTime.get(Calendar.MONTH);
        timeMonth_from.setSelection(month);

        et_toHours.setText(currentTime.get(Calendar.HOUR)+"");
        et_toMinutes.setText(currentTime.get(Calendar.MINUTE)+"");
        et_toDay.setText(currentTime.get(Calendar.DAY_OF_MONTH)+ "");
        et_toYear.setText(currentTime.get(Calendar.YEAR)+"");
        timeAM_PM_to.setSelection(am_pm);
        timeMonth_to.setSelection(month);

        timeAM_PM_from_sleep.setSelection(1);
        timeAM_PM_to_sleep.setSelection(0);
        et_fromHours_sleep.setText("10");
        et_fromMinutes_sleep.setText("0");
        et_toHours_sleep.setText("8");
        et_toMinutes_sleep.setText("0");




        final LinearLayout ringToneContainer = (LinearLayout) findViewById(R.id.alarm_ringtone_container);
        ringToneContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                startActivityForResult(intent , 1);
            }
        });
    }
}

