package ru.itis.llang.fragments;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.adapters.WordsPairsSpinner;
import ru.itis.llang.fragments.alarm.AlarmDetailsFragment;
import ru.itis.llang.fragments.train.TestTrainFragment;
import ru.itis.llang.model.Words;
import ru.itis.llang.repositories.WordsRepository;

public class TrainMainFragment extends Fragment implements View.OnClickListener{

    private final String TAG = "TrainMainFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private View view;
    private Context mContext;
    private FragmentTransaction mFragmentTransaction;
    private Fragment fragment;
    private String tag;

    private Spinner themesSpinner;
    private WordsRepository wordsDAO;
    private WordsPairsSpinner mAdapter;
    private Words words;
    private String tableName = "";
    private ArrayList<String> listOfTableThemeNames;
    private Button next, alarmSet,testTrain,trainDelete,themeADD;
    private ImageButton add_newTheme;

    private OnFragmentInteractionListener mListener;

    public TrainMainFragment() {
    }

    public static TrainMainFragment newInstance() {
        TrainMainFragment fragment = new TrainMainFragment();
        return fragment;
    }

    public static TrainMainFragment newInstance(String param1, String param2) {
        TrainMainFragment fragment = new TrainMainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private class ViewHolder{
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
    }
    private ViewHolder vh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_train_main, container, false);
        mContext = getActivity().getBaseContext();
        mFragmentTransaction = getActivity().getFragmentManager().beginTransaction();

      /*  add_newTheme = (ImageButton) view.findViewById(R.id.ibAdd_newTheme);
        next = (Button) view.findViewById(R.id.ibNext);
        alarmSet = (Button) view.findViewById(R.id.alarmSet);
        testTrain = (Button) view.findViewById(R.id.ibTestTrain);
        trainDelete = (Button) view.findViewById(R.id.ibTrainDelete);
        themeADD = (Button) view.findViewById(R.id.themeADD);

        add_newTheme.setOnClickListener(this);
        next.setOnClickListener(this);
        alarmSet.setOnClickListener(this);
        testTrain.setOnClickListener(this);
        trainDelete.setOnClickListener(this);
        themeADD.setOnClickListener(this);

        wordsDAO = new WordsRepository(mContext);
        Log.d(TAG, " wordsDAO = new WordsDAO(this)");
        final ArrayList<String> listOfTableThemeNames = wordsDAO.allThemesTablesNames();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, listOfTableThemeNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        themesSpinner = (Spinner) view.findViewById(R.id.spinnerOfThemes);
        Log.d(TAG, "size" + listOfTableThemeNames.size());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        themesSpinner.setAdapter(adapter);
        themesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
                long name = themesSpinner.getSelectedItemId();
                tableName = listOfTableThemeNames.get((int)name) + "_theme";
                CurrentSettings.set_current_theme(mContext, tableName);
                Toast.makeText(mContext, "Position = " + tableName, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });*/
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        boolean isFragment = true;
        switch (v.getId()) {
          /*  case R.id.ibAdd_newTheme:
                tag = getResources().getString(R.string.navigation_drawer_all_words);
                fragment = AllWordsFragment.newInstance();
                break;*/
            /*case R.id.ibNext:
                tag = getResources().getString(R.string.navigation_drawer_train_words);
                fragment = TrainWordsFragment.newInstance();
                break;*/
            case R.id.alarmSet:
                /*tag = getResources().getString(R.string.button_title_alarm);
                fragment = AlarmDetailsFragment.newInstance();*/
                isFragment = false;
                startActivity(new Intent(getActivity(), AlarmDetailsFragment.class));
                break;
            case R.id.ibTestTrain:
                tag = getResources().getString(R.string.button_title_test);
                fragment = TestTrainFragment.newInstance();
                break;
            case R.id.ibTrainDelete:
                //tag = getResources().getString(R.string.button_title_remove_theme);
                //fragment = TrainWordsFragment.newInstance();
                break;
            case R.id.themeADD:
                /*tag = getResources().getString(R.string.button_title_add_words);
                fragment = TrainWordsFragment.newInstance();*/
                break;
        }
        if (isFragment) replaceFragment(fragment, tag);
    }


    private void replaceFragment(Fragment fragment,  String tag){
        mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.content_frame, fragment,
                tag);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    private void initView(){
        vh = new ViewHolder();
        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_train_words));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();
    }

}
