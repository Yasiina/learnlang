package ru.itis.llang.fragments.alarm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import ru.itis.llang.R;

/**
 * Created by yasina on 14.04.16.
 */
public class AlarmRecyclerViewAdapter extends RecyclerView.Adapter<AlarmRecyclerViewAdapter.RowHolder>{

    private static final String TAG = "AlarmAdapter";
    private List<AlarmModel> mList;
    private RecyclerView mRecyclerView;
    private Context context;
    private View view;

    public AlarmRecyclerViewAdapter(List<AlarmModel> mAlarms, Context context) {
        this.mList = mAlarms;
        this.context = context;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.temp_recycler_view_item_menu, null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(RowHolder rowHolder, int i) {
        final AlarmModel alarm = mList.get(i);

        rowHolder.tvThemeName.setText(alarm.getThemeName());

       // if (alarm.get){
        rowHolder.switchAlarmButton.setVisibility(View.INVISIBLE);
        String fromAlarm = alarm.getFromDay() + "." + alarm.getFromMonth() + "." + alarm.getFromYear()
                + alarm.getFromHours() + ":" + alarm.getFromMinutes() + " " + alarm.getFromAM_PM();
        rowHolder.tvFromAlarmTime.setText(fromAlarm);
        rowHolder.tvThemeName.setText(alarm.getThemeName());
        rowHolder.tvRepeatTime.setText(alarm.getRepeat() +" " + alarm.getRepeatMin_Hour());
        String toAlarm = alarm.getToDay() + "." + alarm.getToMonth() + "." + alarm.getToYear()
                + alarm.getToHours() + ":" + alarm.getToMinutes() + " " + alarm.getToAM_PM();
        rowHolder.tvToAlarmTime.setText(toAlarm);

    }



    @Override
    public int getItemCount() {
        return (null != mList ? mList.size() : 0);
    }

    public static class RowHolder extends RecyclerView.ViewHolder{

        private TextView tvThemeName, tvRepeatTime, tvFromAlarmTime, tvToAlarmTime;
        private View v;
        private Switch switchAlarmButton;

        public RowHolder(View view) {
            super(view);
            this.v = view;
            tvThemeName = (TextView) view.findViewById(R.id.tv_alarm_title_theme);
            switchAlarmButton = (Switch) view.findViewById(R.id.switch_alram);
            tvRepeatTime = (TextView) view.findViewById(R.id.tv_alarm_title_repeat_amount_number);
            tvFromAlarmTime = (TextView) view.findViewById(R.id.tv_alarm_title_from_number);
            tvToAlarmTime = (TextView) view.findViewById(R.id.tv_alarm_title_to_number);
        }



    }
}
