package ru.itis.llang.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.adapters.DictionariesSpinner;
import ru.itis.llang.fragments.base.BaseFragment;
import ru.itis.llang.model.Dictionary;
import ru.itis.llang.repositories.DictionaryRepository;
import ru.itis.llang.utils.CurrentSettings;

public class SettingsFragment extends android.app.Fragment{

    private final String TAG = "MainFragment";
    private View view;
    private Context mContext;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private BaseFragment mBaseFragment;

    private DictionaryRepository dictionaryDAO;
    private List<Dictionary> listDictionaries;
    private DictionariesSpinner mDictionariesSpinnerAdapter;

    private class ViewHolder{
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        Spinner mLanSpinner, mSpinnerDictionaries;
        ImageView mAddDictionaryImageView;

        AutoCompleteTextView mFirstLanguage;
        AutoCompleteTextView mSecondLanguage;
    }
    private ViewHolder vh;

    public SettingsFragment() {}

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        initView();
        return view;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initView() {
        vh = new ViewHolder();
        mContext = getActivity().getBaseContext();

        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        vh.mLanSpinner = (Spinner) view.findViewById(R.id.languages_spinner);
        setLanguageSpinner();
        vh.mSpinnerDictionaries = (Spinner) view.findViewById(R.id.spinnerOfDictionaries);
        setDictionaryList();
        vh.mAddDictionaryImageView = (ImageView) view.findViewById(R.id.add_dictionary_image_view);
        setAddingNewDictionary();

        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.action_settings));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();

    }

    private void setDictionaryList(){
        dictionaryDAO = new DictionaryRepository(mContext);
        try {
            listDictionaries = dictionaryDAO.getAllDictionaries();

            for (Dictionary i : listDictionaries) {
                Log.d(TAG, i.getName());
            }

            if (listDictionaries != null) {
                mDictionariesSpinnerAdapter = new DictionariesSpinner(mContext, listDictionaries);
                mDictionariesSpinnerAdapter.notifyDataSetChanged();
                vh.mSpinnerDictionaries.setAdapter(mDictionariesSpinnerAdapter);
                vh.mSpinnerDictionaries.setVerticalScrollbarPosition(0);
                vh.mSpinnerDictionaries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        CurrentSettings.set_current_languages(mContext, listDictionaries.get(position).getName());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }catch (RuntimeException e){

        }
    }

    private void setLanguageSpinner(){
        vh.mLanSpinner = (Spinner) view.findViewById(R.id.languages_spinner);
        List<String> list = new ArrayList<String>();
        list.add("Русский");
        list.add("English");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        vh.mLanSpinner.setAdapter(adapter);
        vh.mLanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CurrentSettings.setSettingsLanguage(mContext, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setAddingNewDictionary(){
        vh.mAddDictionaryImageView.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(mContext);
                final View mDialogView = li.inflate(R.layout.dialog_dicitionary, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getResources().getString(R.string.text_dictionary_dialog))
                        .setView(mDialogView)
                        .setNegativeButton(getResources().getString(R.string.save),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                            addDictionary(mDialogView);
                                            dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void addDictionary(View mDialogView){
        vh.mFirstLanguage = (AutoCompleteTextView) mDialogView.findViewById(R.id.first_edit_text_dictionary_dialog);
        vh.mSecondLanguage = (AutoCompleteTextView) mDialogView.findViewById(R.id.second_edit_text_dictionary_dialog);

        Editable firstLang = vh.mFirstLanguage.getText();
        Editable secondLang = vh.mSecondLanguage.getText();

        //TODO: change "translate"
        String clicked = "translate";

        if (!TextUtils.isEmpty(firstLang) && !TextUtils.isEmpty(secondLang)) {
            String name = firstLang.toString() + "_" + secondLang.toString();

            DictionaryRepository mDictionaryRepository = new DictionaryRepository(getActivity().getBaseContext());
            mDictionaryRepository.add(name, clicked);


            listDictionaries = dictionaryDAO.getAllDictionaries();
            mDictionariesSpinnerAdapter.notifyDataSetChanged();
            Toast.makeText(getActivity().getBaseContext(), getString(R.string.successfully_added_new_dicitionary), Toast.LENGTH_LONG).show();
        }
        else {
        }

    }
}
