package ru.itis.llang.fragments;

import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.StringTokenizer;

import ru.itis.llang.R;
import ru.itis.llang.model.Dictionary;
import ru.itis.llang.repositories.DictionaryRepository;
import ru.itis.llang.repositories.WordsRepository;


public class AddDictionaryFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String ADD_DICTIONARY_FRAGMENT_TAG = "add_dictionary_fragment";

    private EditText mTxtFirstLang, mTxtSecondLang;
    private Button mBtnAdd;
    private String[] types = {"translate","synonyms","homonyms","explanatory"};
    private Spinner spinner;
    private DictionaryRepository mDictionaryRepository;
    private ArrayAdapter<String> adapter;
    private String clicked;
    private long mPosition;
    private long mClick;
    private static final String ARG_PARAM1 = "mClick";
    private static final String ARG_PARAM2 = "mPosition";
    private View mView;

    private OnFragmentInteractionListener mListener;

    public AddDictionaryFragment() {
    }

    public static AddDictionaryFragment newInstance(long click, long position) {
        AddDictionaryFragment fragment = new AddDictionaryFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_PARAM1, click);
        args.putLong(ARG_PARAM2, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddDictionaryFragment newInstance() {
        AddDictionaryFragment fragment = new AddDictionaryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mClick = getArguments().getLong(ARG_PARAM1);
            mPosition = getArguments().getLong(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_dictionary, container, false);
        initViews();

        try{
            Dictionary dic = mDictionaryRepository.getDicitonaryById(mClick);
            StringTokenizer tokenizer = new StringTokenizer(dic.getName(), "-");

            while (tokenizer.hasMoreElements()) {
                mTxtFirstLang.setText(tokenizer.nextToken());
                mTxtSecondLang.setText(tokenizer.nextToken());
            }

            int c = 0;
            for(int i=0;i<types.length;i++){
                if(types[i].equals(dic.getType())){
                    c = i;
                }
            }
            spinner.setSelection(c);

        }catch (RuntimeException e){
        }

        return mView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initViews() {
        this.mTxtFirstLang = (EditText) mView.findViewById(R.id.txt_firstLanguage_add_new_dic);
        this.mTxtSecondLang = (EditText) mView.findViewById(R.id.txt_secondLanguage_add_new_dic);
        this.mBtnAdd = (Button) mView.findViewById(R.id.btn_addNewDictionaryTable_add_new_d);
        this.mBtnAdd.setOnClickListener(this);

        this.mDictionaryRepository = new DictionaryRepository(getActivity().getBaseContext());

        adapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner = (Spinner) mView.findViewById(R.id.spinner_TypeOfDictionaries_add_new_d);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_addNewDictionaryTable_add_new_d:
                Editable firstLang = mTxtFirstLang.getText();
                Editable secondLang = mTxtSecondLang.getText();
                clicked = spinner.getSelectedItem().toString();
                if (!TextUtils.isEmpty(firstLang) && !TextUtils.isEmpty(secondLang)) {
                    String name = firstLang.toString() + "_" + secondLang.toString();

                    mDictionaryRepository = new DictionaryRepository(getActivity().getBaseContext());
                    mDictionaryRepository.add(name, clicked);

                    WordsRepository mWordsRepository = new WordsRepository(getActivity().getBaseContext(), name);

                  /*  try{
                        mDictionaryRepository.deleteDictionary(mClick);
                    }catch (RuntimeException e){
                        System.out.println("no dic");
                    }*/
                    // Dictionary createdDictionary = mDictionaryRepository.createDictionary(
                    //       firstLang.toString() + "_" + secondLang.toString(),clicked);

                    //Log.d(TAG, "added dictionary : " + createdDictionary.getName());
                    changeFragment();

                    Toast.makeText(getActivity().getBaseContext(), "dictionary_created_successfully", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getActivity().getBaseContext(),"empty_fields_message", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        clicked = spinner.getSelectedItem().toString();
        Log.d(ADD_DICTIONARY_FRAGMENT_TAG, "clickedItem : " + clicked);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDictionaryRepository.close();
    }

    private void changeFragment(){
        FragmentManager mFragmentManager = getActivity().getFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        // AddDictionaryFragment mFragmentDictionary = AddDictionaryFragment.newInstance();
        mFragmentTransaction.remove(mFragmentManager.findFragmentByTag(AddDictionaryFragment.ADD_DICTIONARY_FRAGMENT_TAG));
        mFragmentTransaction.commit();
    }
}