package ru.itis.llang.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.adapters.ThemesAdapter;
import ru.itis.llang.fragments.base.BaseFragment;
import ru.itis.llang.repositories.WordsRepository;

/**
 * Created by yasina on 04.05.16.
 */
public class ThemesFragment extends Fragment implements ThemesAdapter.OnItemClickListener{

    public static final String TAG = "ThemesFragment";
    private View view;
    private ArrayList<String> mThemeList;
    private WordsRepository mWordsDAO;
    private Context mContext;
    private ThemesAdapter mThemeAdapter;
    private BaseFragment mBaseFragment;

    public ThemesFragment(){}

    public static ThemesFragment newInstance() {
        ThemesFragment fragment = new ThemesFragment();
        return fragment;
    }

    @Override
    public void onItemClick(String viewModel) {
        AllWordsFragment mFragment = AllWordsFragment.newInstance(viewModel, mContext);
        mBaseFragment.replaceFragment(mFragment, AllWordsFragment.TAG);
    }

    private class ViewHolder{
        RecyclerView mListviewWP;
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        FloatingActionButton mAddFab;
    }

    private ViewHolder vh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_words, container, false);
        setHasOptionsMenu(true);

        vh = new ViewHolder();

        try {
            initViews();

        }catch (RuntimeException e){
            view = inflater.inflate(R.layout.empty, container, false);

        }

        return view;
    }

    private void initViews(){
        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        mContext = getActivity().getBaseContext();
        mWordsDAO = new WordsRepository(mContext);

        vh.mListviewWP = (RecyclerView) view.findViewById(R.id.list_wps);


        mThemeList = mWordsDAO.allThemesTablesNames();
        for(int i=0; i< mThemeList.size(); i++)
            Log.d(TAG, mThemeList.get(i));

        mThemeAdapter = new ThemesAdapter(mThemeList);
        mThemeAdapter.setOnItemClickListener(this);

        vh.mListviewWP.setAdapter(mThemeAdapter);
        vh.mListviewWP.setLayoutManager(new LinearLayoutManager(mContext));

        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_themes));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        vh.mAddFab = (FloatingActionButton) view.findViewById(R.id.new_theme_fab);
        vh.mAddFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();
    }

}
