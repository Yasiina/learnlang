package ru.itis.llang.fragments.train;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.adapters.TestTrainAdapter;
import ru.itis.llang.adapters.listener.TestTrainClickListener;
import ru.itis.llang.fragments.base.BaseFragment;
import ru.itis.llang.model.Words;
import ru.itis.llang.repositories.WordsRepository;

public class TestTrainFragment extends Fragment {

    private final String TAG = "TestTrainFragment";
    private View view;
    private Context mContext;
    private BaseFragment mBaseFragment;
    private TestTrainAdapter mTestTrainAdapter;
    private ArrayList<String> mThemeList;

    private OnFragmentInteractionListener mListener;

    private LinearLayout layout;
    private String name;
    private WordsRepository themeWordsDAO;
    private List<Words> words;
    private TextView[] textView;
    private LinearLayout[] ll;
    private EditText[] editText;
    private int[] randoms;
    private Button checkButton, againButton, backButton;
    private int allWordsCount;
    private int size, n;
    private ArrayList<Integer> mas;
    private Random random;
    private LinearLayout.LayoutParams params;

    public TestTrainFragment() {}

    public static TestTrainFragment newInstance() {
        TestTrainFragment fragment = new TestTrainFragment();
        return fragment;
    }

    private class ViewHolder{
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        FloatingActionButton mDoneFab;
        RecyclerView mWordsRecyclerView;

        Spinner mThemesSpinner;
    }
    private ViewHolder vh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity().getBaseContext();
        vh = new ViewHolder();
        chooseThemeDialog();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.temp_train, container, false);
        initView();

        return view;
    }

    private void initView(){
        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_train_words));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();

        vh.mDoneFab = (FloatingActionButton) view.findViewById(R.id.done_fab);
        vh.mDoneFab.setVisibility(View.INVISIBLE);


        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        //vh.mWordsRecyclerView = (RecyclerView) view.findViewById(R.id.list_wps);
       // vh.mWordsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));

    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void chooseThemeDialog(){
        themeWordsDAO = new WordsRepository(mContext, name);
        mThemeList = themeWordsDAO.allThemesTablesNames();

        LayoutInflater li = LayoutInflater.from(mContext);
        final View mDialogView = li.inflate(R.layout.dialog_choose_theme, null);

        vh.mThemesSpinner = (Spinner)
                mDialogView.findViewById(R.id.themes_spinner);
        setSpinner();

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.choose_theme))
                .setView(mDialogView)
                .setNegativeButton(getResources().getString(R.string.save),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void setSpinner(){
        themeWordsDAO = new WordsRepository(mContext, name);
        mThemeList = themeWordsDAO.allThemesTablesNames();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                android.R.layout.simple_spinner_item, mThemeList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vh.mThemesSpinner.setAdapter(adapter);
        vh.mThemesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
                name = mThemeList.get(position)  + "_theme";
                afterDialog();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void printWords(){

        themeWordsDAO = new WordsRepository(mContext, name);
        words = new ArrayList<Words>();
        words = themeWordsDAO.getAllDictionaries();
        size = words.size();
        Log.d("Train", words.size() + " name=" + name);

        layout = (LinearLayout) view.findViewById(R.id.linearLayout2);
        layout.setOrientation(LinearLayout.VERTICAL);

        textView = new TextView[size];
        ll = new LinearLayout[size];
        editText = new EditText[size];

        randoms = new int[size];
        mas = new ArrayList<Integer>();
        for(int j = 0; j< size; j++){
            mas.add(j);
        }

        n = size;
        int num;
        random = new Random();
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,30,0,0);
        for (int i = 0; i < size; i++) {

            num = random.nextInt(n - i);
            randoms[i] = mas.remove(num);

            textView[i] = new TextView(mContext);
            textView[i].setTextColor(getResources().getColor(R.color.cardview_dark_background));
            textView[i].setLayoutParams(params);
            textView[i].setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
            textView[i].setText(words.get(randoms[i]).getFirstLang());
            textView[i].setId(i);
            textView[i].setEms(10);

            editText[i] = new EditText(mContext);
            editText[i].setTextColor(getResources().getColor(R.color.cardview_dark_background));
            editText[i].setId(i);
            editText[i].setEms(10);

            ll[i] = new LinearLayout(mContext);
            ll[i].setOrientation(LinearLayout.HORIZONTAL);
            ll[i].addView(textView[i]);
            ll[i].addView(editText[i]);

            layout.addView(ll[i]);
        }

       /* mTestTrainAdapter = new TestTrainAdapter(newListWords);
        mTestTrainAdapter.setOnItemClickListener(new TestTrainAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Words viewModel) {

            }
        });
        vh.mWordsRecyclerView.setAdapter(mTestTrainAdapter);
        vh.mWordsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        /*vh.mWordsRecyclerView.addOnItemTouchListener( // and the click is handled
                new TestTrainClickListener(getActivity(), new TestTrainClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Log.d("click item", String.valueOf(position));
                        AutoCompleteTextView textView = (AutoCompleteTextView) view.findViewById(R.id.item_actv);
                        Log.d("test",textView.getText().toString());
                    }
                }));
        vh.mWordsRecyclerView.setRecyclerListener(new RecyclerView.RecyclerListener() {
            @Override
            public void onViewRecycled(RecyclerView.ViewHolder holder) {
                //AutoCompleteTextView textView = (AutoCompleteTextView) view.findViewById(R.id.item_actv);
                AutoCompleteTextView textView = (AutoCompleteTextView) holder.itemView.findViewById(R.id.item_actv);
                Log.d("test",textView.getText().toString());
            }
        });*/

        vh.mDoneFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < size; i++) {
                    String answer = editText[i].getText().toString();
                    int temp = randoms[i];
                    if (!answer.equals(words.get(temp).getSecondLang())) {
                        editText[i].setBackgroundColor(Color.RED);
                    }
                }
            }
        });

        /*checkButton = new Button(mContext);
        checkButton.setEms(10);
        params.setMargins(120,100,0,0);
        checkButton.setLayoutParams(params);
        checkButton.setBackgroundResource(R.drawable.buttonshape2);
        checkButton.setText("Check");
        checkButton.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < size; i++) {
                    String answer = editText[i].getText().toString();
                    int temp = randoms[i];
                    if (!answer.equals(words.get(temp).getSecondLang())) {
                        editText[i].setBackgroundColor(Color.RED);
                    }
                }
                try {
                    layout.removeView(againButton);
                }catch (RuntimeException e){

                }
                againButton = new Button(mContext);
                againButton.setText("Again");
                againButton.setEms(10);
                params.setMargins(120,30,0,0);
                againButton.setLayoutParams(params);
                againButton.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
                againButton.setBackgroundResource(R.drawable.buttonshape2);
                againButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                layout.addView(againButton);

            }
        });
        layout.addView(checkButton);*/

    }

     private void afterDialog(){
        vh.mDoneFab.setVisibility(View.VISIBLE);
        printWords();
    }


}

