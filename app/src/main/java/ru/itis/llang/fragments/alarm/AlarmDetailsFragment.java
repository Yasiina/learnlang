package ru.itis.llang.fragments.alarm;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import ru.itis.llang.R;
import ru.itis.llang.utils.CurrentSettings;

public class AlarmDetailsFragment extends Fragment{

    public static final String TAG = "AlarmDetailsFragment";
    private AlarmModel alarm;
    private TextView txtToneSelection;
    private Context mContext;
    private View view;

    private EditText mRepeatTimeEditText;
    private Spinner mRepeatTimeSpinner;
    private TextView mThemeNameTextView;
    private TextView mFromRepeatTimeTextView, mToRepeatTimeTextView,
            mFromSleepTimeTextView, mToSleepTimeTextView, mSaveTextView;

    private Calendar mFrom, mTo, mFromSleep, mToSleep;
    private DatePickerDialog mDatePickerDialog;
    private TimePickerDialog mTimePickerDialog;

    private int cur;
    private String name;
    private Uri alarmTone;
    private int rep_min_hour;
    private boolean old_alarm = false;
    private AlarmDAO alarmDAO;

    public AlarmDetailsFragment(){}

    public static AlarmDetailsFragment newInstance(){
        AlarmDetailsFragment alarmDetailsFragment = new AlarmDetailsFragment();
        return alarmDetailsFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.temp_alarm_details, container, false);

        mContext = getActivity().getBaseContext();
        alarmDAO = new AlarmDAO(mContext);

        mThemeNameTextView = (TextView) view.findViewById(R.id.dictionary_name_alarm_details);
        name = CurrentSettings.get_current_theme(mContext);
        String temp = name.replace("_theme","");
        mThemeNameTextView.setText(temp);

        initView();

        try{
            alarm = alarmDAO.get(name);
            txtToneSelection.setText(RingtoneManager.getRingtone(mContext, Uri.parse(alarm.alarmTone)).getTitle(mContext));
            old_alarm = true;
            Log.d("alala", "old alarm");

        }catch (RuntimeException e){
            alarm = new AlarmModel();
            alarm.setThemeName(name);
            old_alarm = false;
            Log.d("alala", "new alarm");

        }
        cur = 0;
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (resultCode == RESULT_OK) {
           // switch (requestCode) {
             //   case 1: {
                    alarm.alarmTone = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI).toString();
                    txtToneSelection.setText(RingtoneManager.getRingtone(mContext, alarmTone).getTitle(mContext));
               //     break;
                //}
                //default: {
               //     break;
                //}
            //}
        //}
    }


    private void setAlarmParameters(){

        alarm.setFromHours(mFrom.get(Calendar.HOUR_OF_DAY));
        alarm.setFromMinutes(mFrom.get(Calendar.MINUTE));
        alarm.setFromDay(mFrom.get(Calendar.DAY_OF_MONTH));
        alarm.setFromMonth(mFrom.get(Calendar.MONTH));
        alarm.setFromYear(mFrom.get(Calendar.YEAR));
        String am_pm = "pm";
        if (mFrom.get(Calendar.AM_PM) == 0) am_pm = "am";
        alarm.setFromAM_PM(am_pm);

        alarm.setToHours(mTo.get(Calendar.HOUR_OF_DAY));
        alarm.setToMinutes(mTo.get(Calendar.MINUTE));
        alarm.setToDay(mTo.get(Calendar.DAY_OF_MONTH));
        alarm.setToMonth(mTo.get(Calendar.MONTH));
        alarm.setToYear(mTo.get(Calendar.YEAR));
        if (mTo.get(Calendar.AM_PM) == 0) am_pm = "am";
        alarm.setToAM_PM(am_pm);

        alarm.setFromSleepHours(mFromSleep.get(Calendar.HOUR_OF_DAY));
        alarm.setFromSleepMinutes(mFromSleep.get(Calendar.MINUTE));
        if (mTo.get(Calendar.AM_PM) == 0) am_pm = "am";
        alarm.setFromSleep_AM_PM(am_pm);

        alarm.setToSleepHours(mToSleep.get(Calendar.HOUR_OF_DAY));
        alarm.setToSleepMinutes(mToSleep.get(Calendar.MINUTE));
        if (mTo.get(Calendar.AM_PM) == 0) am_pm = "am";
        alarm.setToSleep_AM_PM(am_pm);

        int repeat = Integer.parseInt(mRepeatTimeEditText.getText().toString());
        if(rep_min_hour == 1) repeat = repeat*60;
        alarm.setRepeat(repeat);
        String rep = mRepeatTimeSpinner.getSelectedItem().toString();
        alarm.setRepeatMin_Hour(rep);

        alarm.setEnabled(true);

    }

    private void initView(){

        mFromRepeatTimeTextView = (TextView) view.findViewById(R.id.tv_date_from_alarm_time_details);
        mToRepeatTimeTextView = (TextView) view.findViewById(R.id.tv_date_to_alarm_time_details);
        mFromSleepTimeTextView  = (TextView) view.findViewById(R.id.tv_date_from_sleep_time);
        mToSleepTimeTextView  = (TextView) view.findViewById(R.id.tv_date_to_sleep_time);
        mRepeatTimeSpinner = (Spinner) view.findViewById(R.id.spinner_repeat_min_hour);
        mRepeatTimeEditText = (EditText) view.findViewById(R.id.editText_every_alarm_details);
        txtToneSelection = (TextView) view.findViewById(R.id.alarm_label_tone_selection);

        mFromSleepTimeTextView.setText("22:00");
        mFromSleep = Calendar.getInstance();
        mFromSleep.set(Calendar.HOUR_OF_DAY, 22);
        mFromSleep.set(Calendar.MINUTE, 0);

        mToSleepTimeTextView.setText("07:00");
        mToSleep = Calendar.getInstance();
        mToSleep.set(Calendar.HOUR_OF_DAY, 7);
        mToSleep.set(Calendar.MINUTE, 0);
        mToSleep.set(Calendar.AM_PM, 0);

        mFromRepeatTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFrom = showChooseDateDialogs(mFromRepeatTimeTextView);
            }
        });
        mToRepeatTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTo = showChooseDateDialogs(mToRepeatTimeTextView);
            }
        });
        mFromSleepTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFromSleep = showChooseDateDialogs(mFromSleepTimeTextView);
            }
        });
        mToSleepTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mToSleep = showChooseDateDialogs(mToSleepTimeTextView);
            }
        });

        mSaveTextView = (TextView) view.findViewById(R.id.save_text_view_alarm_details);
        mSaveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAlarmParameters();

                AlarmManagerHelper.cancelAlarms(mContext);

                if (old_alarm) {
                    alarmDAO.updateAlarm(alarm);
                    Log.d("alala", "update old alarm");
                } else {
                    alarmDAO.createAlarm(alarm);
                    Log.d("alala", "create new alarm");
                }
                AlarmManagerHelper.setAlarms(mContext);
                alarmDAO.close();
                //setResult(RESULT_OK);
                //finish();
            }
        });


        final LinearLayout ringToneContainer = (LinearLayout) view.findViewById(R.id.alarm_ringtone_container);
        ringToneContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                startActivityForResult(intent, 1);
            }
        });
    }

    private Calendar showChooseDateDialogs(final TextView mTextView){

        final Calendar currentTime = Calendar.getInstance();
        int mYear = currentTime.get(Calendar.YEAR);
        int mMonth = currentTime.get(Calendar.MONTH);
        int mDay = currentTime.get(Calendar.DAY_OF_MONTH);

        mDatePickerDialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        final String s = dayOfMonth + "." + (monthOfYear + 1) + "." + year;
                        currentTime.set(Calendar.YEAR, year);
                        currentTime.set(Calendar.MONTH, monthOfYear);
                        currentTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        int mHour = currentTime.get(Calendar.HOUR_OF_DAY);
                        int mMinute = currentTime.get(Calendar.MINUTE);

                        mTimePickerDialog = new TimePickerDialog(mContext,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        mTextView.setText(s + " " + hourOfDay + ":" + minute);
                                        currentTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        currentTime.set(Calendar.MINUTE, minute);
                                    }
                                }, mHour, mMinute, false);
                        mTimePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        mTimePickerDialog.show();

                    }
                }, mYear, mMonth, mDay);
        mDatePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mDatePickerDialog.show();
        return currentTime;
    }
}

