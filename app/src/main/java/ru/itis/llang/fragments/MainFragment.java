package ru.itis.llang.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.fragments.alarm.AlarmDAO;
import ru.itis.llang.fragments.alarm.AlarmModel;
import ru.itis.llang.fragments.alarm.AlarmRecyclerViewAdapter;
import ru.itis.llang.fragments.alarm.AlarmDetailsFragment;
import ru.itis.llang.fragments.base.BaseFragment;


public class MainFragment extends Fragment{

    private final String TAG = "MainFragment";
    private View view;
    private Context mContext;
    private OnFragmentInteractionListener mListener;

    private AlarmDAO mAlarmDAO;
    private List<AlarmModel> mCurrentModels;
    private AlarmRecyclerViewAdapter mRecyclerViewAdapter;
    private BaseFragment mBaseFragment;

    private class ViewHolder{
        FloatingActionButton mAddAlarmFab;
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        TextView mFromSleepTime, mToSleepTime;
        RecyclerView mAlarmsRecyclerView;
    }
    private ViewHolder vh;

    public MainFragment() {}

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        initView();
        return view;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initView(){
        vh = new ViewHolder();
        mContext = getActivity().getBaseContext();
        mAlarmDAO = new AlarmDAO(mContext);
        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        vh.mAddAlarmFab = (FloatingActionButton) view.findViewById(R.id.add_alarm_button);
        vh.mAddAlarmFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*AddDictionaryFragment mFragmentDictionary = AddDictionaryFragment.newInstance();
                mBaseFragment.replaceFragment(mFragmentDictionary, AddDictionaryFragment.ADD_DICTIONARY_FRAGMENT_TAG);*/
                AlarmDetailsFragment mAlarmDetailsFragment = AlarmDetailsFragment.newInstance();
                mBaseFragment.replaceFragment(mAlarmDetailsFragment, AlarmDetailsFragment.TAG);
            }
        });

        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_main));
        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();

        vh.mFromSleepTime = (TextView) view.findViewById(R.id.tv_date_from_sleep_time);
        vh.mFromSleepTime = (TextView) view.findViewById(R.id.tv_date_to_sleep_time);

        mCurrentModels = mAlarmDAO.getAlarms();
        vh.mAlarmsRecyclerView = (RecyclerView) view.findViewById(R.id.rv_alarms_main_fragment);
        vh.mAlarmsRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerViewAdapter = new AlarmRecyclerViewAdapter(mCurrentModels, mContext);
        vh.mAlarmsRecyclerView.setAdapter(mRecyclerViewAdapter);


    }
}
