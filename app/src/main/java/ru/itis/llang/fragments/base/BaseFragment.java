package ru.itis.llang.fragments.base;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;

import ru.itis.llang.R;
import ru.itis.llang.fragments.AddDictionaryFragment;

/**
 * Created by yasina on 11.05.16.
 */
public class BaseFragment {

    private FragmentManager mFragmentManager;

    public BaseFragment(FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }

    public void replaceFragment(Fragment mFragment, String tag){
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.content_frame, mFragment).addToBackStack(tag);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    public void onBackClick(View view){
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener( new View.OnKeyListener()
        {
            @Override
            public boolean onKey( View v, int keyCode, KeyEvent event )
            {
                if( keyCode == KeyEvent.KEYCODE_BACK )
                {
                    if (mFragmentManager.getBackStackEntryCount() == 0) {
                        return false;
                    } else {
                        mFragmentManager.popBackStack();
                        return true;
                    }
                }
                return false;
            }
        } );
    }
}
