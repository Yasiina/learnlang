package ru.itis.llang.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.MainActivity;
import ru.itis.llang.R;
import ru.itis.llang.adapters.AllWordsAdapter;
import ru.itis.llang.fragments.base.BaseFragment;
import ru.itis.llang.model.Dictionary;
import ru.itis.llang.model.Words;
import ru.itis.llang.repositories.DictionaryRepository;
import ru.itis.llang.repositories.WordsRepository;
import ru.itis.llang.utils.CurrentSettings;


public class AllWordsFragment extends Fragment
        implements AllWordsAdapter.OnItemClickListener {

    private View view;
    public static final String TAG = "AllWordsFragment";

    private AllWordsAdapter mAdapter;
    private ArrayList<Words> mListWP;
    private WordsRepository mWordsPairDao;
    private Context mContext;
    private String tableName;
    private List<Words> mThemeList;
    private BaseFragment mBaseFragment;

    public AllWordsFragment(){}

    public static AllWordsFragment newInstance(String name, Context mContext) {
        AllWordsFragment fragment;
        String tableName;
        if(name.equals("all")){
            tableName = CurrentSettings.get_current_languages(mContext);
        }else {
            tableName = name + "_theme";
        }
        fragment = new AllWordsFragment(tableName);

        return fragment;
    }

    public AllWordsFragment(String tableName) {
        this.tableName = tableName;
    }

    private class ViewHolder{
        RecyclerView mListviewWP;
        Toolbar toolbar;
        ActionBarDrawerToggle toggle;
        FloatingActionButton mAddFab, mSaveFab;

        AutoCompleteTextView mNewThemeActv;
    }

    private ViewHolder vh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_words, container, false);
        mContext = getActivity().getBaseContext();
        setHasOptionsMenu(true);

        vh = new ViewHolder();

        try {
            DictionaryRepository dicDAO = new DictionaryRepository(getActivity());

            Log.d(TAG,tableName);
            initViews();

            mWordsPairDao = new WordsRepository(getActivity(), tableName);

            mListWP = mWordsPairDao.getAllDictionaries();
            if (mListWP != null && !mListWP.isEmpty()) {

                mAdapter = new AllWordsAdapter(tableName, mListWP);
                mAdapter.setOnItemClickListener(this);
                vh.mListviewWP.setAdapter(mAdapter);
                mThemeList = new ArrayList<Words>();

            } else {
                vh.mListviewWP.setVisibility(View.GONE);

            }
        }catch (RuntimeException e){
            view = inflater.inflate(R.layout.empty, container, false);
        }

        return view;
    }

    private void initViews() {
        mBaseFragment = new BaseFragment(getFragmentManager());
        mBaseFragment.onBackClick(view);

        vh.mListviewWP = (RecyclerView) view.findViewById(R.id.list_wps);
        vh.mListviewWP.setLayoutManager(new LinearLayoutManager(mContext));
        vh.toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        ((AppCompatActivity)getActivity()).setSupportActionBar(vh.toolbar);
        vh.toolbar.setBackgroundColor(getResources().getColor(R.color.for_toolbar));

        vh.toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        initFabs();
        vh.toggle = new ActionBarDrawerToggle(
                getActivity(), MainActivity.drawer, vh.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        MainActivity.drawer.setDrawerListener(vh.toggle);
        vh.toggle.syncState();


    }

    @Override
    public void onItemClick(Words viewModel) {
        Log.d(TAG, "clickedItem : " + viewModel.getFirstLang());
    }

    @Override
    public void onCheckBoxClick(boolean isChecked,int id) {
        Log.d(TAG, "clickedItem : " + id);
        Words w = mListWP.get(id);
        Log.d(TAG, "clickedItem : " + w.getFirstLang());
        if(isChecked)
            mThemeList.add(w);
        else
            mThemeList.remove(id);
    }

    private void initFabs(){
        vh.mSaveFab = (FloatingActionButton) view.findViewById(R.id.save_theme_fab);
        setOnClickSaveFab();
        vh.mAddFab = (FloatingActionButton) view.findViewById(R.id.new_theme_fab);

        if(!tableName.contains("_theme")){
            vh.mAddFab.setVisibility(View.INVISIBLE);
            vh.toolbar.setTitle(getResources().getString(R.string.navigation_drawer_all_words));
        } else {
            vh.toolbar.setTitle(tableName);
            vh.mAddFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddWordsFragment mFragment = AddWordsFragment.newInstance();
                    mBaseFragment.replaceFragment(mFragment,AddWordsFragment.ADD_WORDS_FRAGMENT_TAG);
                }
            });
        }
    }


    private void setOnClickSaveFab(){
        vh.mSaveFab.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(mContext);
                final View mDialogView = li.inflate(R.layout.dialog_new_theme, null);

                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                builder.setTitle(getResources().getString(R.string.text_new_theme_dialog))
                        .setView(mDialogView)
                        .setNegativeButton(getResources().getString(R.string.save),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        vh.mNewThemeActv = (AutoCompleteTextView)
                                                mDialogView.findViewById(R.id.first_edit_text_dictionary_dialog);
                                        String name = vh.mNewThemeActv.getText().toString() + "_theme";
                                        WordsRepository wordsDAO_new = new WordsRepository(mContext,name);
                                        wordsDAO_new.addListOfWords(mThemeList);
                                        Log.d(TAG, "" + wordsDAO_new.getAllDictionaries().size());
                                        dialog.cancel();
                                    }
                                });
                android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
