package ru.itis.llang.repositories;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.database.DBHelper;
import ru.itis.llang.model.Dictionary;

/**
 * Created by yasina on 11.03.15.
 */
public class DictionaryRepository {

    public static final String TAG = "DictionaryRepository";

    private Context mContext;
    private SQLiteDatabase mDatabase;
    private DBHelper mDbHelper;
    private String[] mAllColumns = {DBHelper.COLUMN_NAME_ID, DBHelper.COLUMN_NAME, DBHelper.COLUMN_TYPE};

    private String SQL_CREATE_TABLE_DICTIONARY_PART_1 =
            "CREATE TABLE IF NOT EXISTS ";
    private String SQL_CREATE_TABLE_DICTIONARY_PART_2 ="("
            + DBHelper.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DBHelper.COLUMN_NAME + " TEXT NOT NULL, "
            + DBHelper.COLUMN_TYPE + " TEXT NOT NULL)";
    private String name = "dictionary";

    public DictionaryRepository(Context context, String name) {
        mDbHelper = new DBHelper(context);
        this.mContext = context;
        try {
            open();
            createTable(name);
            this.name = name;
        }
        catch(SQLException e) {
            Log.e(TAG, "SQLException on openning database " + e.getMessage());
            e.printStackTrace();
        }
    }

    public DictionaryRepository(Context context) {
        mDbHelper = new DBHelper(context);
        this.mContext = context;
        open();
    }

    public void open() throws SQLException {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    public void close() {
        mDbHelper.close();
    }

    public void createTable(String sql){
        mDatabase.execSQL(SQL_CREATE_TABLE_DICTIONARY_PART_1 + sql + SQL_CREATE_TABLE_DICTIONARY_PART_2);
        Log.d(TAG, SQL_CREATE_TABLE_DICTIONARY_PART_1 + sql + SQL_CREATE_TABLE_DICTIONARY_PART_2);
    }

    public Dictionary add(String dictionaryName, String type) {
        open();
        ContentValues values = new ContentValues();
        values.put(DBHelper.COLUMN_NAME, dictionaryName);
        values.put(DBHelper.COLUMN_TYPE, type);

        long insertId = mDatabase.insert(DBHelper.TABLE_DICTIONARY, null, values);
        Cursor cursor = mDatabase.query(DBHelper.TABLE_DICTIONARY,
                mAllColumns, DBHelper.COLUMN_NAME_ID + " = " + insertId, null, null, null, null);
        cursor.moveToFirst();
        Dictionary newDicitonary = cursorToDictionary(cursor);
        cursor.close();
        return newDicitonary;
    }

    public void deleteDictionary(Dictionary dictionary) {
        long id = dictionary.getmId();
        System.out.println("the deleted dictionary has the id: " + id);
        mDatabase.delete(DBHelper.TABLE_DICTIONARY, DBHelper.COLUMN_NAME_ID + " = " + id, null);
    }

    public void deleteDictionary(String dictionary) {
        mDatabase.delete(DBHelper.TABLE_DICTIONARY, DBHelper.COLUMN_NAME + " = " + dictionary, null);
    }

    public void deleteDictionary(long dictionary) {
        System.out.println("the deleted dictionary has the id: " + dictionary);
        Dictionary dic = getDicitonaryById(dictionary);
        try {
            mDatabase.delete(DBHelper.TABLE_DICTIONARY, DBHelper.COLUMN_NAME_ID + " = " + dictionary, null);
        }catch (RuntimeException e){
            System.out.println("nothing2");
        }
        try {
            mDatabase.rawQuery("DROP TABLE " + dic.getName(), null);
            System.out.println("DROP TABLE " + dic.getName());
        }catch (RuntimeException e){
            System.out.println("nothing");}

    }

    public List<Dictionary> getAllDictionaries() {
        List<Dictionary> listDictionaries = new ArrayList<Dictionary>();

        Cursor cursor = mDatabase.query(DBHelper.TABLE_DICTIONARY,
                mAllColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Dictionary dictionary = cursorToDictionary(cursor);
            listDictionaries.add(dictionary);
            cursor.moveToNext();
        }
        cursor.close();
        return listDictionaries;
    }



    public Dictionary getDicitonaryById(long id) {
        Cursor cursor = mDatabase.query(DBHelper.TABLE_DICTIONARY,mAllColumns,
                DBHelper.COLUMN_NAME_ID + " = ?",
                new String[] { String.valueOf(id) }, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        Dictionary dictionary = cursorToDictionary(cursor);
        return dictionary;
    }

    public Dictionary getDicitonaryByName(String tableName) {
        Cursor cursor = mDatabase.query(DBHelper.TABLE_DICTIONARY,mAllColumns,
                DBHelper.COLUMN_NAME + " = ?",
                new String[] { tableName }, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        Dictionary dictionary = cursorToDictionary(cursor);
        return dictionary;
    }

    private Dictionary cursorToDictionary(Cursor cursor) {
        Dictionary dictionary = new Dictionary();
        dictionary.setmId(cursor.getLong(0));
        dictionary.setName(cursor.getString(1));
        dictionary.setType(cursor.getString(2));
        return dictionary;
    }

}


