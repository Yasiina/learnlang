package ru.itis.llang.paint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.R;


/**
 * Created by yasina on 25.03.15.
 */
public class DrawingView extends View {

    private final String DRAWING_VIEW_TAG = "drawing_view_tag";
    private Path drawPath;
    private Paint drawPaint, canvasPaint;
    //private int paintColor = 0xFF660000;
    private int paintColor = Color.BLACK;
    private Canvas drawCanvas;
    private Bitmap canvasBitmap;
    private float brushSize, lastBrushSize;
    private boolean erase=false;

    private String text           = "";
    private Typeface fontFamily   = Typeface.DEFAULT;
    //private float fontSize        = 50F;
    private Paint.Align textAlign = Paint.Align.RIGHT;  // fixed
    private Paint textPaint;
    private float textX           = 0F;
    private float textY           = 0F;

    private enum MODE{
        LINE,
        RECTANGLE,
        CIRCLE,
        TEXT
    }
    public DrawingView(Context context, AttributeSet attrs){
        super(context, attrs);
        //setupDrawing();
        setupDrawing();
    }

    public void setBackgroundBitmap(Bitmap map){
        //canvasBitmap = map;
        setBackground(new BitmapDrawable(map));

//            drawCanvas.setBitmap(map);
    }

    private MODE myMode = MODE.LINE;
    private int historyPointer = 0;
    private float startX   = 0F;
    private float startY   = 0F;


    private Paint setupDrawing(){

        brushSize = getResources().getInteger(R.integer.small_size);
        lastBrushSize = brushSize;
        drawPath = new Path();
        drawPaint = new Paint();
        textPaint= new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(brushSize);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        // canvasPaint = new Paint(Paint.DITHER_FLAG);
        drawPaint.setShadowLayer(0F, 0F, 0F, Color.WHITE);
        //drawPaint.setAlpha(255);

        return drawPaint;
    }

    //size assigned to view
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(w>0 && h>0) {
            canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            drawCanvas = new Canvas(canvasBitmap);
        }else {
            Log.e("Drawing_saved_to_g", "Mistake");
        }
    }

    //draw the view - will be called after touch event
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);

        if (myMode == MODE.TEXT)
        {
            drawText(canvas);

        }else {

        }


    }


    Path path;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                   /* //this.onActionDown(event);
                    if (myMode == MODE.RECTANGLE){
                        updateHistory(this.createPath(event));
                    }else {
                        path = this.getCurrentPath();
                        path.reset();
                        path.moveTo(touchX, touchY);
                    }*/
                startX = touchX;
                startY = touchY;
                break;
            case MotionEvent.ACTION_MOVE:

                   /* path = this.getCurrentPath();
                    path.reset();
                    if (myMode == MODE.RECTANGLE){
                        path.addRect(this.startX, this.startY, touchX, touchY, Path.Direction.CCW);
                    }else {
                        path.lineTo(touchX,touchY);
                    }

*/
                this.onActionMove(event);
                break;
            case MotionEvent.ACTION_UP:
                if (myMode == MODE.LINE) {
                    drawPath.lineTo(touchX, touchY);
                }
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }
        //redraw
        invalidate();
        return true;

    }

    public void setText(String text) {
        this.text = text;
    }

    public void setRectangleMode(){
        myMode = MODE.RECTANGLE;
    }

    public void setCircleMode(){
        myMode = MODE.CIRCLE;
    }

    //update color
    public void setColor(String newColor){
        invalidate();
        Log.d("color", paintColor + " is " + newColor);
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);

    }


    //set brush size
    public void setBrushSize(float newSize){
        float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                newSize, getResources().getDisplayMetrics());
        brushSize=pixelAmount;
        drawPaint.setStrokeWidth(brushSize);
    }

    //get and set last brush size
    public void setLastBrushSize(float lastSize){
        lastBrushSize=lastSize;
    }
    public float getLastBrushSize(){
        return lastBrushSize;
    }

    //set erase true or false
    public void setErase(boolean isErase){
        erase=isErase;
        myMode = MODE.LINE;
        drawPath.reset();
        drawPath = new Path();

        drawPaint.setColor(Color.WHITE);

    }

    //start new drawing
    public void startNew(){
        drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    public Paint getPicture(){
        return drawPaint;
    }

    public void setLineMode(){
        myMode = MODE.LINE;
    }

    private void drawText(Canvas canvas) {
        if (this.text.length() <= 0) {
            return;
        }

        if (myMode == MODE.TEXT) {
            this.textX = this.startX;
            this.textY = this.startY;


            Paint paintForMeasureText = new Paint();

            // Line break automatically
            float textLength = paintForMeasureText.measureText(this.text);
            float lengthOfChar = textLength / (float) this.text.length();
            float restWidth = canvas.getWidth() - textX;  // text-align : right
            int numChars = (lengthOfChar <= 0) ? 1 : (int) Math.floor((double) (restWidth / lengthOfChar));  // The number of characters at 1 line
            int modNumChars = (numChars < 1) ? 1 : numChars;
            float y = textY;

            for (int i = 0, len = this.text.length(); i < len; i += modNumChars) {
                String substring = "";

                if ((i + modNumChars) < len) {
                    substring = this.text.substring(i, (i + modNumChars));
                } else {
                    substring = this.text.substring(i, len);
                }

                y += 100;

                canvas.drawText(substring, textX, y, this.textPaint);
            }
        }
    }

    private Path createPath(MotionEvent event) {
        Path path = new Path();

        // Save for ACTION_MOVE
        this.startX = event.getX();
        this.startY = event.getY();

        path.moveTo(this.startX, this.startY);

        return path;
    }

    private void onActionMove(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        Log.d(DRAWING_VIEW_TAG,"mode=" + myMode);
//        Path path = this.getCurrentPath();
        if (myMode == MODE.RECTANGLE) {
            // drawPath.lineTo(x, y);
            setRectangle(x, y);
        }if (myMode == MODE.LINE){
            drawPath.lineTo(x, y);

        }if(myMode == MODE.CIRCLE){
            double distanceX = Math.abs((double)(this.startX - x));
            double distanceY = Math.abs((double)(this.startX - y));
            //double radius    = Math.sqrt(Math.pow(distanceX, 2.0) + Math.pow(distanceY, 2.0));
            double radius    = Math.sqrt(x*y);

            drawPath.reset();
            drawPath.addCircle(this.startX, this.startY, (float)radius, Path.Direction.CCW);
        }if(myMode == MODE.TEXT){
            drawPaint.setTypeface(this.fontFamily);
            drawPaint.setTextSize(100);
            drawPaint.setTextAlign(this.textAlign);
            drawPaint.setStrokeWidth(0F);
        }
    }

    public void setRectangle(float x, float y){
        // Path path = this.getCurrentPath();
        drawPath.reset();
        drawPath.addRect(this.startX, this.startY, x, y, Path.Direction.CCW);
        String s = "";

    }

    public void setTextMode(){
        myMode = MODE.TEXT;
    }


    public void setColor(int newColor){
        invalidate();
        // paintColor = Color.parseColor(newColor);
        drawPaint.setColor(newColor);
    }


}
