package ru.itis.llang.paint;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import java.util.ArrayList;

import ru.itis.llang.R;

/**
 * Created by yasina on 04.05.15.
 */
public class ColorAdapter extends ArrayAdapter {

        public static final String TAG = "ColorAdapter";
        private Context context;
        private ArrayList<MyColor> objects = new ArrayList<MyColor>();

        public ColorAdapter(Context context,int list_item_layout, ArrayList<MyColor> listOfColors) {
            super(context,list_item_layout,listOfColors);
            this.context = context;
            if(listOfColors !=null) this.objects = listOfColors;
        }

        @Override
        public int getCount() {
            return objects.size();
        }

        @Override
        public MyColor getItem(int position) {
            return objects.get(position);
        }

        private ViewHolder vh = new ViewHolder();
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View rowView = convertView;

            if(rowView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.colors, null);

                vh.color_btn = (ImageView) rowView.findViewById(R.id.color_btn);
                vh.color_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "I'm here");
                        MainPaint_Fragment.drawView.setColor(objects.get(position).getColor());
                        vh.color_btn.setImageDrawable(context.getResources().getDrawable(R.drawable.paint_pressed));
                    }
                });

                rowView.setTag(vh);
            }
            ViewHolder vh = (ViewHolder) rowView.getTag();

           // vh.color_btn.setImageResource(R.drawable.paint222);
            /*vh.color_btn.setBackgroundResource(R.drawable.paint222);
            vh.color_btn.setBackgroundColor(objects.get(position).getColor());*/
            //vh.color_btn.setTag(objects.get(position).getColor());
            ShapeDrawable biggerCircle= new ShapeDrawable( new OvalShape());
            biggerCircle.setIntrinsicHeight( 60 );
            biggerCircle.setIntrinsicWidth( 60);
            biggerCircle.setBounds(new Rect(30, 30, 30, 30));
            biggerCircle.getPaint().setColor(objects.get(position).getColor());//you can give any color here
            vh.color_btn.setBackgroundDrawable(biggerCircle);

            return rowView;
        }

        class ViewHolder {
            ImageView color_btn;
        }
}
