package ru.itis.llang.paint;

/**
 * Created by yasina on 04.05.16.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import ru.itis.llang.R;
import ru.itis.llang.fragments.AddWordsFragment;

public class MainPaint_Fragment extends Fragment implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener{

    public static final String MAIN_PAINT_FRAGMENT_TAG = "main_paint_fragment";

    public static DrawingView drawView;
    private ImageButton currPaint, drawBtn, eraseBtn, newBtn, openCamera, saveBtn;
    //private Toolbar mToolbar;
    private FloatingActionButton floatingActionButton;

    private float smallBrush, mediumBrush, largeBrush;
    private ColorRepository colorDAO;
    private ArrayList<MyColor> colors;
    private ColorAdapter colorAdapter;
    private ListView colorsListView;
    private boolean clicked = false;

    private View view;
    //private Toolbar toolbar;
    private String name = "";
    private Context mContext;
    private int mPicType;

    public static MainPaint_Fragment newInstance() {
        MainPaint_Fragment fragment = new MainPaint_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.paint_fragment, container, false);
        setHasOptionsMenu(true);

        floatingActionButton =
                (FloatingActionButton) view.findViewById(R.id.float_save_txt_btn);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setDrawingCacheEnabled(true);
                drawView.buildDrawingCache(true);
                Bitmap bm = drawView.getDrawingCache();
                drawView.setBackground(new BitmapDrawable(bm));
                drawView.setLineMode();
                floatingActionButton.setVisibility(View.INVISIBLE);
            }
        });
        floatingActionButton.setVisibility(View.INVISIBLE);

        mContext = getActivity().getBaseContext();
        openCamera = (ImageButton) view.findViewById(R.id.open_photo_btn);
        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        ImageButton rectangleBtn = (ImageButton) view.findViewById(R.id.rectangle_btn);
        rectangleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setRectangleMode();
            }
        });

        ImageButton circleBtn = (ImageButton) view.findViewById(R.id.circle_btn);
        circleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setCircleMode();
            }
        });

        ImageButton txtBtn = (ImageButton) view.findViewById(R.id.txt_btn);
        txtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(mContext);
                final View mDialogView = li.inflate(R.layout.dialog_edit_text, null);

                final EditText editText = (EditText)
                        mDialogView.findViewById(R.id.edittext_paint);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Write here your word")
                        .setView(mDialogView)
                        .setNegativeButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        name = editText.getText().toString();
                                        drawView.setText(name);
                                        drawView.setTextMode();
                                        dialog.cancel();
                                        floatingActionButton.setVisibility(View.VISIBLE);
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


        newBtn = (ImageButton) view.findViewById(R.id.new_btn);
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder newDialog = new AlertDialog.Builder(getActivity());
                newDialog.setTitle("New drawing");
                newDialog.setMessage("Start new drawing (you will lose the current drawing)?");
                newDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        drawView.startNew();
                        dialog.dismiss();
                    }
                });
                newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                clicked = false;
                newDialog.show();
            }
        });


        drawBtn = (ImageButton) view.findViewById(R.id.draw_btn);
        // TODO: ??? drawView.setBrushSize(smallBrush);
        drawBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked = true;
                final Dialog brushDialog = new Dialog(getActivity());
                brushDialog.setTitle("Brush size:");
                brushDialog.setContentView(R.layout.brush_chooser);
                ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
                smallBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(false);
                        drawView.setBrushSize(smallBrush);
                        drawView.setLastBrushSize(smallBrush);
                        brushDialog.dismiss();
                    }
                });
                ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
                mediumBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(false);
                        drawView.setBrushSize(mediumBrush);
                        drawView.setLastBrushSize(mediumBrush);
                        brushDialog.dismiss();
                    }
                });
                ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
                largeBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(false);
                        drawView.setBrushSize(largeBrush);
                        drawView.setLastBrushSize(largeBrush);
                        brushDialog.dismiss();
                    }
                });
                brushDialog.show();

            }
        });
        eraseBtn = (ImageButton) view.findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog brushDialog = new Dialog(getActivity());
                brushDialog.setTitle("Eraser size:");
                brushDialog.setContentView(R.layout.brush_chooser);
                ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
                smallBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(true);
                        drawView.setBrushSize(smallBrush);
                        brushDialog.dismiss();
                    }
                });
                ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
                mediumBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(true);
                        drawView.setBrushSize(mediumBrush);
                        brushDialog.dismiss();
                    }
                });
                ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
                largeBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        drawView.setErase(true);
                        drawView.setBrushSize(largeBrush);
                        brushDialog.dismiss();
                    }
                });
                brushDialog.show();
            }
        });

        saveBtn = (ImageButton) view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setDrawingCacheEnabled(true);
                Bitmap bm = drawView.getDrawingCache();

                if(bm != null)
                {
                    ////TODO
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    AddWordsFragment mFragment;
                    try{
                        Log.d("hi", bm.getWidth() + " - " + bm.getHeight());
                        Log.d("hi", "im here");

                        bm.compress(Bitmap.CompressFormat.PNG, 100, bos);
                        mFragment = AddWordsFragment.newInstance(bos.toByteArray());

                    }catch (RuntimeException e) {

                    }

                    ////TODO

                    FragmentTransaction mFragmentTransaction = getFragmentManager().beginTransaction();
                    Fragment f  = getFragmentManager().findFragmentByTag(MAIN_PAINT_FRAGMENT_TAG);
                    mFragmentTransaction.remove(f).commit();
                    //getFragmentManager().popBackStack();

                }else {
                    AlertDialog.Builder warningDialog = new AlertDialog.Builder(getActivity());
                    warningDialog.setTitle("Warning!");
                    warningDialog.setMessage("You are not painting picture! Need to create something.");
                    warningDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    warningDialog.show();
                }

            }
        });


        colorDAO = new ColorRepository(getActivity().getApplicationContext());
        currPaint = new ImageButton(getActivity().getApplicationContext());
        colorsListView = (ListView) view.findViewById(R.id.paint_colors);

        try{
            colors = colorDAO.getAll();
            Log.d(MAIN_PAINT_FRAGMENT_TAG, "4");
        }catch (RuntimeException e){
            colors = new ArrayList<MyColor>();
            Log.d(MAIN_PAINT_FRAGMENT_TAG, "5");
        }


        if(colors.size() > 0){
            colorAdapter =  new ColorAdapter(getActivity().getApplicationContext(), R.layout.colors, colors);
            colorsListView.setAdapter(colorAdapter);
            colorsListView.setOnItemClickListener(this);
        }
        drawView = (DrawingView) view.findViewById(R.id.drawing);
        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        return view;
    }
    public byte[] getPicture()
    {
        drawView.setDrawingCacheEnabled(true);
        drawView.buildDrawingCache(true);

        Bitmap bitmap = Bitmap.createBitmap(drawView.getWidth(), drawView.getHeight(), Bitmap.Config.ARGB_8888);
        ByteArrayOutputStream stream = null;

        if(bitmap!=null && clicked){
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        }
        else{
            Toast unsavedToast = Toast.makeText(getActivity().getApplicationContext(),
                    "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
            unsavedToast.show();
        }
        drawView.destroyDrawingCache();

        return stream.toByteArray();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d(MAIN_PAINT_FRAGMENT_TAG, "1");
        currPaint = (ImageButton) adapterView.getItemAtPosition(i);
        Log.d(MAIN_PAINT_FRAGMENT_TAG, "2");
        currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
        Log.d(MAIN_PAINT_FRAGMENT_TAG, "3");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*int color = data.getExtras().getInt("color");
        drawView.setColor(color);
        colorDAO.add(color);

        try{
            colorDAO = new ColorRepository(getActivity().getApplicationContext());
            colors = colorDAO.getAll();
        }catch (RuntimeException e){
            colors = new ArrayList<MyColor>();
        }

        colorAdapter =  new ColorAdapter(getActivity().getApplicationContext(), R.layout.colors, colors);
        colorsListView.setAdapter(colorAdapter);
        colorsListView.setOnItemClickListener(this);*/
        if (mPicType == 0)
            onCaptureImageResult(data);
        else if(mPicType == 1){
            onSelectFromGalleryResult(data);
        }

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    mPicType = 0;
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 0);
                } else if (items[item].equals("Choose from Library")) {
                    mPicType = 1;
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            1);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        drawView.setBackgroundBitmap(thumbnail);

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);


        drawView.setBackgroundBitmap(bm);

    }
}



