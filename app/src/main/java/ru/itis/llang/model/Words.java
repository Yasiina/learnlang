package ru.itis.llang.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by yasina on 10.03.15.
 */
public class Words implements Parcelable {

    public static final String TAG = "WordsPair";

    private long mId;
    private String firstLang, secondLang, explanation;
    private byte[] image;


    public Words() {
    }

    public Words(String firstLang, String secondLang,  byte[] image,String explanation) {
        this.firstLang = firstLang;
        this.secondLang = secondLang;
        this.explanation = explanation;
        this.image = image;
    }

    protected Words(Parcel in) {
        mId = in.readLong();
        firstLang = in.readString();
        secondLang = in.readString();
        explanation = in.readString();
        image = in.createByteArray();
    }

    public static final Creator<Words> CREATOR = new Creator<Words>() {
        @Override
        public Words createFromParcel(Parcel in) {
            return new Words(in);
        }

        @Override
        public Words[] newArray(int size) {
            return new Words[size];
        }
    };

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public long getmId() {
        return mId;
    }

    public String getFirstLang() {
        return firstLang;
    }

    public String getSecondLang() {
        return secondLang;
    }

    public void setFirstLang(String firstLang) {
        this.firstLang = firstLang;
    }

    public void setmId(long mId) {
        this.mId = mId;
    }

    public void setSecondLang(String secondLang) {
        this.secondLang = secondLang;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(firstLang);
        dest.writeString(secondLang);
        dest.writeString(explanation);
        dest.writeByteArray(image);
    }
}
