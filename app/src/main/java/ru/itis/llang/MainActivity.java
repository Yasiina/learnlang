package ru.itis.llang;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import ru.itis.llang.fragments.AddWordsFragment;
import ru.itis.llang.fragments.AllWordsFragment;
import ru.itis.llang.fragments.MainFragment;
import ru.itis.llang.fragments.SettingsFragment;
import ru.itis.llang.fragments.ThemesFragment;
import ru.itis.llang.fragments.train.TestTrainFragment;
import ru.itis.llang.utils.CurrentSettings;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    public static String MAIN_FRAGMENT_TAG = "main_fragment";

    private String mCurrentDictionaryName;

    private Fragment fragment;
    private String tag;
    private FragmentTransaction mFragmentTransaction;
    public static DrawerLayout drawer;

    private TextView mCurrentDictionary;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCurrentDictionaryName = CurrentSettings.get_current_languages(getBaseContext());
        mFragmentTransaction = getFragmentManager().beginTransaction();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        initView();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        mCurrentDictionary = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvCurrentLang);
        mCurrentDictionary.setText(CurrentSettings.get_current_languages(getBaseContext()));
        navigationView.setNavigationItemSelectedListener(this);

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.for_toolbar));
    }

    private void initView(){
        fragment = MainFragment.newInstance();
        tag = getResources().getString(R.string.navigation_drawer_main);
        replaceFragment(fragment, tag);
    }

    @Override
    public void onBackPressed() {
        //drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            fragment = SettingsFragment.newInstance();
            tag = getResources().getString(R.string.action_settings);
            replaceFragment(fragment, tag);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
      /*  //TODO: get dictionary name inside of fragment
        String name = CurrentSettings.get_current_languages(getBaseContext());

        /// TODO: TEMP!!!
        StringTokenizer tokenizer = new StringTokenizer(name,"_");
        Log.d(MAIN_FRAGMENT_TAG, name);
        String firstL = "";
        String secondL = "";

        while (tokenizer.hasMoreElements()) {
            firstL = tokenizer.nextToken();
            Log.d(MAIN_FRAGMENT_TAG, firstL);
            secondL = tokenizer.nextToken();
            Log.d(MAIN_FRAGMENT_TAG, secondL);
        }
        /// TODO: TEMP!!!*/

        switch (id){
            case R.id.navigation_drawer_main:
                fragment = MainFragment.newInstance();
                tag = getResources().getString(R.string.navigation_drawer_main);
                break;
            case R.id.navigation_drawer_all_words:

                fragment = AllWordsFragment.newInstance("all", getBaseContext());
                tag = getResources().getString(R.string.navigation_drawer_all_words);
                break;

            case R.id.navigation_drawer_add_words:

                fragment = AddWordsFragment.newInstance();
                tag = getResources().getString(R.string.navigation_drawer_add_words);
                break;

            case R.id.navigation_drawer_train_words:
                /*fragment = TrainMainFragment.newInstance();
                tag = getResources().getString(R.string.navigation_drawer_train_words);*/
                tag = getResources().getString(R.string.button_title_test);
                fragment = TestTrainFragment.newInstance();
                break;

            case R.id.navigation_drawer_themes:
                fragment = ThemesFragment.newInstance();
                tag = getResources().getString(R.string.navigation_drawer_themes);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        replaceFragment(fragment, tag);
        return true;
    }

    private void replaceFragment(Fragment fragment,  String tag){
        mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.content_frame, fragment,
                tag);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }


}