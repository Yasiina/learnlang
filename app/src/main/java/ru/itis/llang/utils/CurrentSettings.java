package ru.itis.llang.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Locale;

/**
 * Created by yasina on 30.11.15.
 */
public class CurrentSettings {

    public static final String AUTH_SHARED_PREFS = "ru.itis.llang.utils.auth_shared_prefs";
    public static final String CURRENT_LANGUAGES = "ru.itis.llang.utils.current_languages";
    public static final String CURRENT_THEME = "ru.itis.llang.utils.current_theme";
    public static final String CURRENT_SETTINGS_LANGUAGE = "ru.itis.llang.utils.current_settings_language";

    public static String get_current_languages(Context context){
        SharedPreferences prefs = context.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);
        String token = prefs.getString(CURRENT_LANGUAGES, "");
        return token;
    }

    public static void set_current_languages(Context context, String current_languages){
        SharedPreferences prefs = context.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CURRENT_LANGUAGES, current_languages);
        editor.apply();
    }

    public static String get_current_theme(Context context){
        SharedPreferences prefs = context.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);
        String token = prefs.getString(CURRENT_THEME, "");
        return token;
    }

    public static void set_current_theme(Context context, String current_languages){
        SharedPreferences prefs = context.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CURRENT_THEME, current_languages);
        editor.apply();
    }

    public static void setSettingsLanguage(Context mContext, int position){
        SharedPreferences sPref = mContext.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);

        if(position==0){
            Resources res = mContext.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            conf.locale = new Locale("ru");
            res.updateConfiguration(conf, dm);

            SharedPreferences.Editor ed = sPref.edit();
            ed.putInt(CURRENT_SETTINGS_LANGUAGE, position);
            ed.commit();

        }else{
            Resources res = mContext.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            conf.locale = new Locale("en");
            res.updateConfiguration(conf, dm);

            SharedPreferences.Editor ed = sPref.edit();
            ed.putInt(CURRENT_SETTINGS_LANGUAGE, position);
            ed.commit();
        }
    }

    public static String getSettingsLanguage(Context mContext){
        SharedPreferences prefs = mContext.getSharedPreferences(AUTH_SHARED_PREFS, Activity.MODE_PRIVATE);
        String token = prefs.getString(CURRENT_SETTINGS_LANGUAGE, "");
        return token;
    }

}
