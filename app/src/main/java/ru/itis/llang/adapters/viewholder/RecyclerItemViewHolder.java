package ru.itis.llang.adapters.viewholder;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import ru.itis.llang.R;


public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {

    private TextView txtFirstLang;
    private TextView txtSecondLang;
    private ImageView ivPicture;
    private TextView txtExplanation;
    private CheckBox cbAddWord;

    private RecyclerItemViewHolder(View itemView, TextView txtFirstLang, TextView txtSecondLang,
                                   ImageView ivPicture, TextView txtExplanation, CheckBox cbAddWord) {
        super(itemView);
        this.txtFirstLang = txtFirstLang;
        this.txtSecondLang = txtSecondLang;
        this.ivPicture = ivPicture;
        this.txtExplanation = txtExplanation;
        this.cbAddWord = cbAddWord;
    }

    public static RecyclerItemViewHolder newInstance(View parent) {
        TextView txtFirstLang = (TextView) parent.findViewById(R.id.txt_word_list_item_wp);
        TextView txtSecondLang = (TextView) parent.findViewById(R.id.txt_translate_list_item_wp);
        ImageView ivPicture = (ImageView) parent.findViewById(R.id.ivPicture_list_item_wp);
        TextView txtExplanation = (TextView) parent.findViewById(R.id.txt_explanation_item_wp);
        CheckBox cbAddWord = (CheckBox) parent.findViewById(R.id.add_item_check_box);
        return new RecyclerItemViewHolder(parent, txtFirstLang, txtSecondLang, ivPicture, txtExplanation, cbAddWord);
    }


    public void setTxtFirstLang(String txtFirstLang) {
        this.txtFirstLang.setText(txtFirstLang);
    }


    public void setTxtSecondLang(String txtSecondLang) {
        this.txtSecondLang.setText(txtSecondLang);
    }

    public void setIvPicture(Bitmap ivPicture) {
        this.ivPicture.setImageBitmap(ivPicture);
    }

    public void setTxtExplanation(String txtExplanation) {
        this.txtExplanation.setText(txtExplanation);
    }

    public void setChecked(){
        boolean isEnabled = cbAddWord.isEnabled();
        if(isEnabled)
            cbAddWord.setEnabled(false);
        else
            cbAddWord.setEnabled(true);
    }

    public CheckBox getCbAddWord() {
        return cbAddWord;
    }
}
