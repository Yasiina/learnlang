package ru.itis.llang.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.R;
import ru.itis.llang.adapters.viewholder.RecyclerItemViewHolder;
import ru.itis.llang.model.Words;

/**
 * Created by yasina on 11.03.15.
 */
public class AllWordsAdapter extends RecyclerView.Adapter<RecyclerItemViewHolder>{

    public static final String TAG = "ListWordsPairAdapter";
    private List<Words> mItemList;
    private Context mContext;
    private OnItemClickListener onItemClickListener;
    private View view;
    private String tableType;

    public AllWordsAdapter(String tableType,ArrayList<Words> listDictionaries) {
        this.mItemList = listDictionaries;
        this.tableType = tableType;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        view = LayoutInflater.from(mContext).inflate(R.layout.list_item_words_pair, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick((Words) v.getTag());
            }
        });
        return RecyclerItemViewHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerItemViewHolder holder, final int position) {

        Words item = mItemList.get(position);
        holder.setTxtFirstLang(item.getFirstLang());
        holder.setTxtSecondLang(item.getSecondLang());

        byte[] outImage = mItemList.get(position).getImage();
        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);

        holder.setIvPicture(Bitmap.createScaledBitmap(theImage,500,500, true));

        holder.setTxtExplanation(mItemList.get(position).getExplanation());
        holder.itemView.setTag(item);

        final CheckBox chBox = holder.getCbAddWord();
        Log.d(TAG, "tableName=" + tableType);
        if(!tableType.contains("_theme"))
            chBox.setVisibility(View.INVISIBLE);
        else {
            chBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean isChecked = chBox.isChecked();
                    onItemClickListener.onCheckBoxClick(isChecked,position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public interface OnItemClickListener {

        void onItemClick(Words viewModel);
        void onCheckBoxClick(boolean isChecked, int id);

    }

}


