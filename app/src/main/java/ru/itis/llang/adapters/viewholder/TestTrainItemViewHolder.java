package ru.itis.llang.adapters.viewholder;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AutoCompleteTextView;

import ru.itis.llang.R;

/**
 * Created by yasina on 25.05.16.
 */
public class TestTrainItemViewHolder extends RecyclerView.ViewHolder {

    private AutoCompleteTextView mWordItemActv;

    private TestTrainItemViewHolder(View itemView, AutoCompleteTextView themeName) {
        super(itemView);
        this.mWordItemActv = themeName;
    }

    public static TestTrainItemViewHolder newInstance(View parent) {
        @SuppressLint("WrongViewCast") 
        AutoCompleteTextView txtFirstLang = (AutoCompleteTextView) parent.findViewById(R.id.item_actv);
        return new TestTrainItemViewHolder(parent, txtFirstLang);
    }

    public String getText(String word) {
        return this.mWordItemActv.getText().toString();
    }
    public void setText(String word) {
        this.mWordItemActv.setText(word);
    }
    public void setHint(String word) {
        this.mWordItemActv.setHint(word);
    }
    public String getText(){return this.mWordItemActv.getText().toString();}
}
