package ru.itis.llang.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.R;
import ru.itis.llang.adapters.viewholder.RecyclerItemViewHolder;
import ru.itis.llang.adapters.viewholder.ThemeItemViewHolder;

/**
 * Created by yasina on 04.05.16.
 */
public class ThemesAdapter extends RecyclerView.Adapter<ThemeItemViewHolder> implements View.OnClickListener{


    public static final String TAG = "ThemesAdapter";
    private List<String> mItemList;
    private Context mContext;
    private OnItemClickListener onItemClickListener;
    private View view;

    public ThemesAdapter(ArrayList<String> listDictionaries) {
        this.mItemList = listDictionaries;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ThemeItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        view = LayoutInflater.from(mContext).inflate(R.layout.list_theme_item, parent, false);
        view.setOnClickListener(this);
        return ThemeItemViewHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(ThemeItemViewHolder holder, int position) {

        String item = mItemList.get(position);
        holder.setThemeName(item);
        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override public void onClick(View v) {
        Object c = v.getTag();
        Log.d(TAG, c.toString());
        onItemClickListener.onItemClick(c.toString());
    }

    public interface OnItemClickListener {

        void onItemClick(String viewModel);

    }

}


