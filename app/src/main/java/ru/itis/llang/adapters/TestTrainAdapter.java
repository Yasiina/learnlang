package ru.itis.llang.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.itis.llang.R;
import ru.itis.llang.adapters.viewholder.TestTrainItemViewHolder;
import ru.itis.llang.model.Words;

/**
 * Created by yasina on 25.05.16.
 */
public class TestTrainAdapter extends RecyclerView.Adapter<TestTrainItemViewHolder> implements View.OnClickListener{


    public static final String TAG = "TestTrainAdapter";
    private ArrayList<Words> mItemList;
    private Context mContext;
    private OnItemClickListener onItemClickListener;
    private View view;

    public TestTrainAdapter(ArrayList<Words> listDictionaries) {
        this.mItemList = listDictionaries;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public TestTrainItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        view = LayoutInflater.from(mContext).inflate(R.layout.item_test, parent, false);
        view.setOnClickListener(this);
        return TestTrainItemViewHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(TestTrainItemViewHolder holder, int position) {
        Words item = mItemList.get(position);
        holder.setHint(item.getFirstLang());
        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    @Override public void onClick(View v) {
        onItemClickListener.onItemClick((Words) v.getTag());
    }

    public interface OnItemClickListener {
        void onItemClick(Words viewModel);

    }

}



