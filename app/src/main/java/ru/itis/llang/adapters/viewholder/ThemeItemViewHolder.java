package ru.itis.llang.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.itis.llang.R;

/**
 * Created by yasina on 04.05.16.
 */
public class ThemeItemViewHolder extends RecyclerView.ViewHolder {

    private TextView themeName;

    private ThemeItemViewHolder(View itemView, TextView themeName) {
        super(itemView);
        this.themeName = themeName;
    }

    public static ThemeItemViewHolder newInstance(View parent) {
        TextView txtFirstLang = (TextView) parent.findViewById(R.id.txt_theme_list_item);
        return new ThemeItemViewHolder(parent, txtFirstLang);
    }


    public void setThemeName(String themeName) {
        this.themeName.setText(themeName);
    }

}

